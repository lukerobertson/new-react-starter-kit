// Development config.

module.exports = {
    // [error,warn,info,verbose,debug,silly]
    loggingLevel: 'error',
    colorize: true,

    webpack: 'development',
    jwtSecret: 'secret'
}
