import { observable, action } from 'mobx'

class MainStore {
    @observable title = ''

    @action
    updateTitle = location => {
        this.title = location
    }
}

const store = new MainStore()

export default store
