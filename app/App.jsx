import React, { Component } from 'react'
import _ from 'lodash'
import css from './App.scss'

const App = props => <div>{props.children}</div>

// this file is not really needed, but good way to wrap the whole app

export default App
