import React, { Component } from 'react'
import { observer } from 'mobx-react'
import MainStore from '../../stores/MainStore'

const Main = () => <p>{MainStore.title}</p>

export default observer(Main)
// wrap in observer so it watches the store
