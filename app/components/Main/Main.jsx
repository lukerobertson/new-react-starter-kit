import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { observable, action } from 'mobx'
import MainStore from '../../stores/MainStore'
import Title from '../Title/Title'
import './Main.scss'

class Main extends React.Component {
	@observable main = '' // data that is watched for changes
	@observable description = '' // data that is watched for changes

	@action
	upateMain = data => {
		// observable should only be changed by actions
		this.main = data
	}

	@action
	updateDescription = data => {
		// observable should only be changed by actions
		this.description = data
	}

	componentDidMount() {
		this.fetchData()
	}

	fetchData = async () => {
		const url =
			'http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22'
		const response = await fetch(url) // get data, NEEDS CORS
		const json = await response.json() // parse json
		MainStore.updateTitle(json.name) // GLOBAL version of this, avaible to anywhere in the app
		const weather = _.head(json.weather) // grab first bit
		this.upateMain(weather.main)
		this.updateDescription(weather.description)
	}

	render() {
		return (
			<div>
				<Title />
				<p>{this.main}</p>
				<p>{this.description}</p>
			</div>
		)
	}
}

export default observer(Main)
