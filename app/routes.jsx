import React from 'react'

import { Router, Route, Switch } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router'
import { Provider } from 'mobx-react'

import MainStore from './stores/MainStore'

import App from './App'

import Main from './components/Main/Main'

const browserHistory = createBrowserHistory()
const routingStore = new RouterStore()

const stores = {
    routing: routingStore,
    global: MainStore
}

const history = syncHistoryWithStore(browserHistory, routingStore)

const Routes = () => {
    return (
        <Provider {...stores}>
            <Router history={history}>
                <Switch>
                    <Route exact path="/no-container" component={Main} />

                    <App>
                        <Switch>
                            <Route exact path="/" component={Main} />

                            <Route component={Main} /> {/* not found route */}
                        </Switch>
                    </App>
                </Switch>
            </Router>
        </Provider>
    )
}

export default Routes
